package se.experis.springwebthymeleaf;

import se.experis.springwebthymeleaf.model.Contact;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class dbHandler {

   private static  String URL ="jdbc:sqlite::resource:contactDB.db";
    private static Connection conn = null;
    public static ArrayList<Contact> contacts =new ArrayList<>();
    public static Contact contact = new Contact();

    public static void openConnection(){
        try{
            conn = DriverManager.getConnection(URL);
            System.out.println("connection to db established");
        }catch (Exception exception){
            System.out.println("Error when open connection" + exception.toString());
        }
        finally {

        }
    }


    public static ArrayList readContacts(){
        openConnection();
        try{
            PreparedStatement preparedStatement =

                    conn.prepareStatement("select id, name, email, contactnumber FROM contacts");
            ResultSet resultSet=preparedStatement.executeQuery();

            while(resultSet.next()){
                contacts.add(
                        new Contact(resultSet.getInt("id"),
                                resultSet.getString("name"),
                                resultSet.getString("email"),
                                resultSet.getString("contactnumber")
                        )   );
            }

            conn.close();
        }catch (Exception exception){
            System.out.println("Error while reading contacts: " + exception);

        }


        finally {
            try{

                System.out.println("Contacts: " + contacts.size());
                conn.close();

            }catch (Exception exception){
                System.out.println(exception);
            }
            return contacts;
        }
    }

    public static ArrayList searchContacts(String searchName){

        if(searchName.equals("")){
            return contacts;
        }

        openConnection();
        searchName="%"+searchName+"%";



        try{
            PreparedStatement preparedStatement =

                    conn.prepareStatement("select id, name, email, contactnumber FROM contacts where name like ?");
                    preparedStatement.setString(1,searchName);

            ResultSet resultSet=preparedStatement.executeQuery();

            while(resultSet.next()){
                contacts.add(
                        new Contact(resultSet.getInt("id"),
                                resultSet.getString("name"),
                                resultSet.getString("email"),
                                resultSet.getString("contactnumber")
                        )   );

            }

            conn.close();
        }catch (Exception exception){
            System.out.println("Error while reading searchcontacts: " + exception);

        }

        finally {
            try{

            }catch (Exception exception){
                System.out.println(exception);
            }
        }

        return contacts;
    }


    public static Contact addContact(Contact contactInput){

        openConnection();
        try{
            PreparedStatement preparedStatement =

                    conn.prepareStatement("INSERT INTO contacts(name,email,contactnumber) values (?,?,?)");

            preparedStatement.setString(1, contactInput.name);
            preparedStatement.setString(2, contactInput.email);
            preparedStatement.setString(3, contactInput.contactnumber);
            preparedStatement.execute();

            conn.close();
        }catch (Exception exception){
            System.out.println(contactInput.name + " Was not inserted into the database " );
            System.out.println("Error while reading searchcontacts: " + exception);

        }

        finally {
            try{
                System.out.println("Contacts: " + contacts.size());
                conn.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }

        return contactInput;
    }

    public static Contact searchByID(int id){
        openConnection();

        try{
            PreparedStatement preparedStatement =

                    conn.prepareStatement("select id, name, email, contactnumber FROM  contacts where id=?");
            preparedStatement.setInt(1,id);

            ResultSet resultSet=preparedStatement.executeQuery();

            while(resultSet.next()){
                contact.setId(resultSet.getInt("id"));
                contact.setName(resultSet.getString("name"));
                contact.setEmail(resultSet.getString("email"));
                contact.setContactnumber(resultSet.getString("contactnumber"));

            }

            conn.close();
        }catch (Exception exception){
            System.out.println("Error while reading searchcontacts by ID: " + exception);

        }

        finally {
            try{

            }catch (Exception exception){
                System.out.println(exception);
            }
        }

        return contact;


    }


    public static String updateName(String name, int id){
        openConnection();
        try{
            PreparedStatement preparedStatement =

                    conn.prepareStatement("update contacts set name=? where id=?;");

            preparedStatement.setString(1, name);
            preparedStatement.setInt(2,id);

            preparedStatement.execute();

            conn.close();
        }catch (Exception exception){
            System.out.println(name + " Was not updated into the database " );
            System.out.println("Error while reading searchcontacts: " + exception);

        }

        finally {
            try{

            }catch (Exception exception){
                System.out.println(exception);
            }
        }



        return "tja";
    }

    public static String updateEmail(String email, int id){
        openConnection();
        try{
            PreparedStatement preparedStatement =

                    conn.prepareStatement("update contacts set email=? where id=?;");

            preparedStatement.setString(1, email);
            preparedStatement.setInt(2,id);

            preparedStatement.execute();

            conn.close();
        }catch (Exception exception){
            System.out.println(email + " Was not updated into the database " );
            System.out.println("Error while reading searchcontacts: " + exception);

        }

        finally {
            try{

            }catch (Exception exception){
                System.out.println(exception);
            }
        }
        return "tja";
    }

    public static String updateContactNumber(String contactNumber, int id){

        openConnection();
        try{
            PreparedStatement preparedStatement =

                    conn.prepareStatement("update contacts set contactnumber=? where id=?;");

            preparedStatement.setString(1, contactNumber);
            preparedStatement.setInt(2,id);

            preparedStatement.execute();

            conn.close();
        }catch (Exception exception){
            System.out.println(contactNumber + " Was not updated into the database " );
            System.out.println("Error while reading searchcontacts: " + exception);

        }

        finally {
            try{

            }catch (Exception exception){
                System.out.println(exception);
            }
        }

        return "tja";
    }

    public static String deleteById(int id){

        openConnection();
        try{
            PreparedStatement preparedStatement =

                    conn.prepareStatement("delete from contacts where id=?");

            preparedStatement.setInt(1,id);

            preparedStatement.execute();

            conn.close();
        }catch (Exception exception){
            System.out.println(id + " Was not deleted " );
            System.out.println("Error while reading searchcontacts: " + exception);

        }

        finally {
            try{

            }catch (Exception exception){
                System.out.println(exception);
            }
        }

        return "åh naj";
    }




}
