package se.experis.springwebthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringwebthymeleafApplication {



	public static void main(String[] args) {
		SpringApplication.run(SpringwebthymeleafApplication.class, args);
	}
}
