package se.experis.springwebthymeleaf.model;

public class Contact {
    public int id;
    public String name;
    public String email;
    public String contactnumber;

    public Contact(){}

    public Contact(int id, String name, String email, String contactnumber) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.contactnumber = contactnumber;
    }
    public Contact(String name, String email, String contactnumber) {
        this.name = name;
        this.email = email;
        this.contactnumber = contactnumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }
}
