package se.experis.springwebthymeleaf.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.springwebthymeleaf.model.Contact;

import java.util.ArrayList;
import static se.experis.springwebthymeleaf.dbHandler.*;

@Controller
public class ContactController {

    private ArrayList contacts = new ArrayList();

    @GetMapping("/contacts")
    public String contact(Model model) {
        if (contacts.size() > 0) {
            contacts.clear();
        }
        contacts = readContacts();
        model.addAttribute("allcontacts", contacts);
        return "allcontacts";
    }

    private ArrayList searchcontacts = new ArrayList();
    @GetMapping("/searchresult")
    public String search(@RequestParam(name = "name")String name, Model model) {
        if (searchcontacts.size() > 0) {
            searchcontacts.clear();
        }
        searchcontacts = searchContacts(name);
        model.addAttribute("searchedcontacts", searchcontacts);
        return "searchcontacts";
    }


    @GetMapping("/addcontact")
    public String addcontact(Model model) {
        return "addcontact";
    }

    @GetMapping("/contacts/{id}")
    public String editContact(@PathVariable int id, Model model) {
        Contact contact = new Contact();
        contact = searchByID(id);
        model.addAttribute("contact", contact);
        return "editcontacts";
    }

    @PostMapping("/contacts/{id}")
    public String updateContact(@RequestParam ("name") String name, @RequestParam ("email") String email, @RequestParam ("contactnumber") String contactnumber, @PathVariable int id) {
        System.out.println(name);

        if (name != null && !name.equals("")) {
            updateName(name, id);
        }
        if (email != null && !email.equals("")) {
            updateEmail(email, id);
        }
        if (contactnumber != null && !contactnumber.equals("")) {
            updateContactNumber(contactnumber, id);
        }
        else {
            return "Contact was not updated.";
        }
        return "index";
    }

    @PostMapping("/delete/{id}")
    public String deleteContact(@PathVariable int id) {
        deleteById(id);
        return "index";
    }

}
