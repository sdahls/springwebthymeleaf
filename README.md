Spring Web/Thymeleaf
-
Oskar Olsson, Sven Dahlström & Tora Haukka

####index.html
Uses navigation bar to display where you can go to all contacts, add contact or search.
This navbar is visable on all htmls. 
If you edit, add or delete you will be redirected to this page.
####addcontact.html
Uses a form to add new contact to the db. 

####allcontacts.html
Displaying all contacts in the db in a table.

####editcontacts.html
Uses a form to display the contact you are editing. 
You can either submit this update or reverse back to previous state. 

####searchcontacts.html
Shows the matching searchresults with the ability to click on the contact if you wish to edit.


Mappings
- 
- GET /contacts - uses an ArrayList to store contacts, and prints out the contacts.
- GET /searchresult - uses an Arraylist to store contacts that matches the search, and prints out the matched contacts.
- GET /addcontacts - used to redirect to addcontact.html
- GET /editcontacts -  used to redirect to editcontacts.html

- POST /contacts/{id} - using a reqparm to get input and updating existing contact. Doesn't update fields that are left empty.
- POST /addcontact - creates a new contact using the class. 
- DELETE /contacts/{id} - deletes contact that has that id. 

###Contributions
Sven took the lead with SQL. 
And with the rest we all helped equally.

###Notes
Due to time restraints, the design is incomplete. 
Also add contacts is not completly finished. 

